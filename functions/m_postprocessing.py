"""Functions for processing measurement data"""

import numpy as np
import math
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    #initializing lists 
    vec_accel = []
    
    #calculating the magnitude of the acceleration vector
    for i in np.arange(len(x)):
        vec_value = math.sqrt(pow(x[i],2)+pow(y[i],2)+pow(z[i],2))
        vec_accel.append(vec_value)
    
    #converting the lists 
    vec_accel = np.array(vec_accel)  
        
    return vec_accel

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    
    #initializing lists 
    n = len(time)
    x = []
    y = []
    
    #creating regularly spaced time intervals
    for i in np.arange(len(time)):
        x.append((time[n-1]/n)*i)
    
    #using numpys interpolate function to linearly interpolate y values
    for i in np.arange(len(x)):
        y_value = np.interp(x[i-1], time, data, left=None, right=None, period=None)
        y.append(y_value)
    
    #converting lists to numpy arrays
    x = np.array(x)
    y = np.array(y)
    
    return x,y

def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    # sampling rate
    sr = len(time)
    #using numpys inbuilt fft function
    X = np.fft.fft(x)
    N = len(X)
    n = np.arange(N)
    T = N/sr
    freq = n/T 
    #removing first value from freq and amplitude, to be able to plot it later
    freq = freq[1:]
    X = X[1:]
    
    # Get the one-sided specturm
    n_oneside = N//2
    # get the one side frequency
    freq = freq[:n_oneside]
    #removing imaginary part of frequency
    amp = np.abs(X[:n_oneside])
    return amp,freq

